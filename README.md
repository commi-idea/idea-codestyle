# idea-codestyle

To apply this codestyle to the project:

1. Copy git url;
1. Go to IDEA settings > Tools > Settings Repository | Read-only sources;
1. Add url and apply changes, wait for IDEA to synchronize;
1. Go to IDEA settings > Editor > Code Style | Scheme;
1. Select CommiCodeStyle and apply changes;